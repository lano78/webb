<!DOCTYPE html>
<head>
	<head>
    <meta charset="UTF-8">
    <title>Elvis AB</title>
    <meta name="viewport" content="width = device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no">
    <link href="./css/style.css" media="screen" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
    <body>
        <header></header>
        <main></main>
        <footer></footer>
    </body>

</html>
